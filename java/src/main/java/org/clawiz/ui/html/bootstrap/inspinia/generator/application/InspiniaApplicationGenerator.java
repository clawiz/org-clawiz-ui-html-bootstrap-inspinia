/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.bootstrap.inspinia.generator.application;


import org.clawiz.ui.common.generator.AbstractApplicationGeneratorContext;
import org.clawiz.ui.common.generator.common.view.form.FormGenerator;
import org.clawiz.ui.common.generator.common.view.grid.GridGenerator;
import org.clawiz.ui.html.bootstrap.common.generator.servlet.application.BootstrapApplicationGenerator;
import org.clawiz.ui.html.bootstrap.inspinia.UiHtmlBootstrapInspiniaModule;
import org.clawiz.ui.html.bootstrap.inspinia.generator.view.form.InspiniaFormGenerator;
import org.clawiz.ui.html.bootstrap.inspinia.generator.view.grid.InspiniaGridGenerator;
import org.clawiz.ui.html.common.generator.servlet.application.component.JavaScriptRequireConfigComponent;

public class InspiniaApplicationGenerator extends BootstrapApplicationGenerator {


    @Override
    public <T extends AbstractApplicationGeneratorContext> Class<T> getApplicationGenerateContextClass() {
        return (Class<T>) InspiniaApplicationGenerateContext.class;
    }

    @Override
    public <T extends FormGenerator> Class<T> getFormGeneratorClass() {
        return (Class<T>) InspiniaFormGenerator.class;
    }

    @Override
    public <T extends GridGenerator> Class<T> getGridGeneratorClass() {
        return (Class<T>) InspiniaGridGenerator.class;
    }

    @Override
    protected <T extends JavaScriptRequireConfigComponent> Class<T> getJavaScriptRequireConfigComponentClass() {
        return (Class<T>) InspiniaJavaScriptRequireConfigComponent.class;
    }

    @Override
    protected void done() {

        addFilesToContextPrototype(UiHtmlBootstrapInspiniaModule.CLAWIZ_ASSETS_RESOURCE_PATH,   "/assets/clawiz/inspinia");
        addFilesToContextPrototype(UiHtmlBootstrapInspiniaModule.INSPINIA_ASSETS_RESOURCE_PATH, "/assets/inspinia");
        super.done();
    }
}
