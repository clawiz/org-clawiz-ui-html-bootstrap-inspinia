/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.bootstrap.inspinia.generator.view.form.servlet.component.element;


import org.clawiz.core.common.system.generator.abstractgenerator.component.element.AbstractElement;
import org.clawiz.ui.html.bootstrap.common.generator.servlet.view.form.servlet.component.element.BootstrapFormColumnElement;
import org.clawiz.ui.html.common.generator.html.element.form.HtmlInputElement;

public class InspiniaFormColumnElement extends BootstrapFormColumnElement {

    protected int getLabelColumnsCount() {
        return 2;
    }

    protected int getRowColumnsCount() {
        return 12;
    }

    @Override
    public void write() {
        boolean labelExists = false;

        for(AbstractElement element : getElements() ) {
            if ( element instanceof HtmlInputElement) {
                if ( ((HtmlInputElement) element).getLabel() != null ) {
                    labelExists = true;
                    break;
                }
            }

        }

        for(AbstractElement element : getElements() ) {
            requestWriteln("<div class=\"form-group\">");
            if ( labelExists ) {

                String label = null;
                if ( element instanceof HtmlInputElement) {
                    label = ((HtmlInputElement) element).getLabel();
                }

                if ( label != null ) {
                    requestWriteln("<label class=\"col-lg-" + getLabelColumnsCount() +" control-label\">" + encodeJavaQuotes(label != null ? label : "") + "</label>");
                    requestWriteln("<div class=\"col-lg-" + (getRowColumnsCount() - getLabelColumnsCount()) + "\">");
                    element.write();
                    requestWriteln("</div>");
                } else {
                    requestWriteln("<div class=\"col-lg-offset-" + getLabelColumnsCount() + " col-lg-" + (getRowColumnsCount() - getLabelColumnsCount())  +"\">");
                    element.write();
                    requestWriteln("</div>");
                }

            } else {

                element.write();

            }
            requestWriteln("</div>");
        }

    }
}
