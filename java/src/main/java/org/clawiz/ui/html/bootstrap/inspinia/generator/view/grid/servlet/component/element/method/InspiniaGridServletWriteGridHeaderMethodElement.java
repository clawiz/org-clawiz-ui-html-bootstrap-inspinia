/*
 *
 *  * MIT License
 *  *
 *  * Copyright (c) 2018 Clawiz
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *  *
 *
 */

package org.clawiz.ui.html.bootstrap.inspinia.generator.view.grid.servlet.component.element.method;


import org.clawiz.ui.common.metadata.data.view.grid.column.AbstractGridColumn;
import org.clawiz.ui.html.bootstrap.common.generator.servlet.view.grid.servlet.component.element.method.BootstrapGridServletWriteGridHeaderMethodElement;

public class InspiniaGridServletWriteGridHeaderMethodElement extends BootstrapGridServletWriteGridHeaderMethodElement {

    @Override
    public void process() {
        super.process();

        addText("request.writeln(\"<table class=\\\"table table-hover\\\">\");");
        addText("request.writeln(\"   <thead>\");");
        addText("request.writeln(\"      <tr>\");");
        for (AbstractGridColumn column : getGrid().getColumns() ) {
            addText("request.writeln(\"         <th>" +  column.getCaption() + "</th>\");");
        }
        addText("request.writeln(\"      </tr>\");");
        addText("request.writeln(\"   </thead>\");");

    }
}
