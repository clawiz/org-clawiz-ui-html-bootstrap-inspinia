/*
 * MIT License
 *
 * Copyright (c) 2017 Clawiz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {StaticComboBoxInputController} from 'clawiz/ui/controller/input/StaticComboBoxInputController'

export class InspiniaDynamicComboBoxInputController extends StaticComboBoxInputController {


    onSelect(value) {

        let input = this.$element.get(0);
        let me    = input.clawizController;

        me.value = value.value;
    }

    doQuery(query, process) {
        let input = this.$element.get(0);
        let me    = input.clawizController;

        let url = me.api.path + '?action=' + me.api.action + '&inputValue=' + query;
        me.logDebug("GET : " + url);
        me.jQuery.get(url, function (result)
        {
            me.logDebug('response : ' + result);
            let data = JSON.parse(result);
            let values = [];
            for(let i=0; i < data.length; i++ ) {
                values.push({
                    name  : data[i].text,
                    value : data[i].value
                });
            }

            return process(values);
        });

    }

    init() {

        let me = this;

        this.jQueryInput.typeahead(
            {
                source: me.doQuery,
                afterSelect : me.onSelect
            }
        );

    }


}