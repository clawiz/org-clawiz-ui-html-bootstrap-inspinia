/*
 * MIT License
 *
 * Copyright (c) 2017 Clawiz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {StaticComboBoxInputController} from 'clawiz/ui/controller/input/StaticComboBoxInputController'

export class InspiniaStaticComboBoxInputController extends StaticComboBoxInputController {

    onSelect(value) {

        let input = this.$element.get(0);
        let controller = input.clawizController;

        controller.value = value.value ;
    }

    init() {

        super.init();

        let me = this;

        let values = [];
        if ( me.values != null ) {
            for(let i=0; i < me.values.length; i++ ) {
                let value = me.values[i];
                if ( value == null ) {
                    continue;
                }
                values.push({
                    name  : value.text,
                    value : value.value
                });
            }
        }

        this.jQueryInput.typeahead(
            {
                source      : values,
                afterSelect : me.onSelect
            }
        );

    }
}